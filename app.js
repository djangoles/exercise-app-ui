const spanMove = document.querySelector('.span-move');
const iconTab = Array.from(document.querySelectorAll('.icon-tab'));
const mover = document.querySelector('.mover');
const tabElevation = document.querySelector('.tab-elevation');
const tabMiles = document.querySelector('.tab-miles');
const tac = document.querySelector('.tac');
const linkMaster  = Array.from(document.querySelectorAll('.links'));
const statTag = document.querySelectorAll('.stat-tag');
const titleHTML = document.querySelector('.stats-3-right h1');
const stats3Para = document.querySelector('.stats-3-right p');
const sortBtn = document.querySelector('.sort-vertical-bars');
let verticalBars = '';
const stats2 = document.querySelector('.stats-2');
const slide4Content = document.querySelector('.slide-4-content');



linkMaster.forEach((link, i) => {
  link.addEventListener('click', (e) => {
    e.preventDefault()
  })
})


const toggleAndMove = (e) => {
  e.preventDefault();

  stats3Para.style.opacity = '0';
  iconTab.forEach((icon) => {
    icon.classList.remove('icon-tab-active');
})

  const tab =  e.target;
  tab.classList.add('icon-tab-active');
  const dataTabID = parseInt(e.target.parentElement.parentElement.dataset.tab);
  const moveDistance = dataTabID * 25;
  spanMove.style.left = `${moveDistance}%`;
  const contentMoveDistance = dataTabID * 700;
  mover.style.transform = `translateX(-${contentMoveDistance}px)`;

  statTag.forEach((tag) => {
    tag.classList.remove('stat-tag-show');
  })
  sortBtn.style.display = 'none';

  if(dataTabID === 1) {
    animateBarLines()
  } else if (dataTabID === 2) {
    typeAndDisplayContent()
  } else if(dataTabID === 3) {
    verticalSteps()
  } else {
    genericCountUp()
  }
}

const verticalSteps = () => {
  slide4Content.textContent = 'This is unsorted, someone please sort me!'
  sortBtn.style.display = 'block';
  const gradients = ['linear-gradient(#0895c0, #9d0cfd)', 'linear-gradient(#fde50c, #c08008)','linear-gradient(#09e7d5, #22aa07)']
  verticalBars = Array.from(document.querySelectorAll('.vertical-bar'));
  verticalBars.forEach((bar) => {
    bar.style.height = '0'; 
      const drawBars = setTimeout(() => {
        let random100 = Math.floor(Math.random() * 300);
        while(random100 < 20) {
          random100 = Math.floor(Math.random() * 300);
        }

        const random = Math.floor(Math.random() * gradients.length);
        bar.dataset.percentage = random100;
        bar.style.height = `${random100}px`;
        bar.style.background = gradients[0];
      }, 300)
  })
}

const typeAndDisplayContent = () => {
  titleHTML.textContent = '';
  const titleText = 'Run For Life';
  let titleResult = ''
  let counter = 0;
  setTimeout(() => {
    const startTyping = setInterval(() => {
      if(counter === titleText.length - 1) {
        clearInterval(startTyping)
      }
      titleResult+= titleText[counter]
      titleHTML.textContent = titleResult;
      counter++
    }, 100)
  }, 400)
  stats3Para.style.opacity = '1';
}

const animateBarLines = () => {
  let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
  const tabLeftNums = document.querySelector('.tab-left-nums');
  const tabRightNums = document.querySelector('.tab-right-nums');
  const tabLeftText = document.querySelector('.tab-left-text');
  const tabRightText = document.querySelector('.tab-right-text');
  let numArray = [];
  
  const bars = Array.from(document.querySelectorAll('.bar'));

  bars.forEach((bar, index) => {
    let random = Math.floor(Math.random() * 100 + 1)
    while(random <  7) {
      random = Math.floor(Math.random() * 100 + 1)
    }

    numArray.push({percentage: random, idx: index});

    bar.firstElementChild.style.opacity = '1';
    bar.firstElementChild.textContent =`${random}%`;
    bar.style.width = `${random}%`;
  })

  let sorted = numArray.sort((a, b) => {
    return a.percentage - b.percentage
  })

  const bestPercent = sorted[sorted.length - 1].percentage;
  const bestDay = days[sorted[sorted.length - 1].idx];
  const worstPercent = sorted[0].percentage;
  const worstDay = days[sorted[0].idx]
  const weeklyHigh = document.querySelector('.weekly-high');
  const weeklyLow = document.querySelector('.weekly-low');
  weeklyHigh.style.cssText = 'opacity:1; width: 100%';
  weeklyLow.style.cssText = 'opacity:1; width: 100%';
  setTimeout(() => {
    statTag.forEach((tag) => {
      tag.classList.add('stat-tag-show');
    })
    tabLeftNums.textContent = `${bestPercent}%`;
    tabRightNums.textContent = `${worstPercent}%`;
    tabLeftText.textContent = bestDay;
    tabRightText.textContent = worstDay;
  },700)
}


const genericCountUp = () => {
  stats2.style.opacity = 1;
  const tabLeftText = document.querySelector('.tab-left-text');
  const tabRightText = document.querySelector('.tab-right-text');
  tabRightText.textContent = 'Elev. Gain (ft)';
  tabLeftText.textContent = 'Pace (min/mi)';

  let counter = 0;

  let generic = setInterval(() => {
    if(counter === 469) {

      clearInterval(generic);
    }

    if(counter < 394) {
      tabElevation.innerHTML = counter;
    }

    if(counter > 99) {
      let counterStr = counter.toString().split('').slice(0, 1) + '.' + counter.toString().split('').slice(1);
      const formatted = counterStr.toString().replace(/\,/, '');
      tabMiles.textContent =  formatted;
    } else {
      let counterStr = counter.toString().split('').slice(0, 1) + counter.toString().split('').slice(1);
      const formatted = counterStr.toString().replace(/\,/, '.');
      tabMiles.textContent =  formatted;
    }
    counter++;
  }, 1)

}


sortBtn.addEventListener('click', (e) => {
  let copy = [...verticalBars];
  let sorted = copy.sort((a, b) => {
    return b.dataset.percentage - a.dataset.percentage
  })

  for (let index = 0; index < verticalBars.length; index++) {
    verticalBars[index].style.height = `0`;
    let newHeight = sorted[index].dataset.percentage;
      verticalBars[index].style.height = `${newHeight}px`;
  }
  slide4Content.textContent = 'Thanks!'
})

iconTab.forEach((icon) => {
  icon.addEventListener('click', toggleAndMove);
})

window.addEventListener('load', () => {

  setTimeout(() => {
    genericCountUp()
  }, 2000)
})


